import 'dart:core';
import 'dart:ui';

import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:flutter/material.dart';
class Category {
  int categoryId;
  List<DoiTuong> diaDanhs;
  String name;
  String image;
  Color color;
  LinearGradient gradient;
  Category({this.name,this.image,this.diaDanhs,this.color,this.gradient});
}