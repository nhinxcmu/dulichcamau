import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';

class DoiTuong {
  int maDoiTuong;
  String tenDoiTuong;
  String diaChi;
  String moTa;
  String hinhAnh;
  int commentCount;
  bool isFavourite;
  DoiTuong(
      {this.maDoiTuong,
      this.tenDoiTuong,
      this.diaChi,
      this.moTa,
      this.hinhAnh,
      this.commentCount,
      this.isFavourite});
  static DoiTuong withDoc(DocumentSnapshot doc) {
    print(doc.data["maDoiTuong"]);
    DoiTuong d = new DoiTuong();
    d.maDoiTuong = int.parse(doc.data["maDoiTuong"].toString());
    d.tenDoiTuong = doc.data["tenDoiTuong"];
    d.diaChi = doc.data["diaChi"];
    d.moTa = doc.data["moTa"];
    d.hinhAnh = doc.data["hinhAnh"];
    d.commentCount = int.parse(doc.data["commentCount"].toString());
    d.isFavourite = doc.data["isFavourite"];
    return d;
  }
}
