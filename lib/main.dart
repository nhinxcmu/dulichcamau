
import 'package:dulichcamau/pages/landingPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("asset/images/background-mainpage.jpg"),
                fit: BoxFit.cover,
                alignment: Alignment(0.2, 1)),
          ),
          alignment: Alignment(0.0, 0.0),
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 150.0),
                        child: Image.asset(
                          "asset/images/logo.png",
                          width: 225.0,
                        ),
                      ),
                      Text(
                        "Du lịch",
                        style: TextStyle(
                            fontSize: 40.0,
                            color: Color.fromRGBO(51, 204, 51, 1),
                            shadows: [
                              BoxShadow(
                                  offset: Offset(2, 2),
                                  blurRadius: 2,
                                  color: Color.fromRGBO(0, 0, 0, 0.5)),
                              BoxShadow(
                                  offset: Offset(2, 2),
                                  blurRadius: 2,
                                  color: Color.fromRGBO(0, 0, 0, 0.5)),
                              BoxShadow(
                                  offset: Offset(2, 2),
                                  blurRadius: 2,
                                  color: Color.fromRGBO(0, 0, 0, 0.5)),
                            ]),
                      ),
                      Text(
                        "Cà Mau",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 45.0,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            shadows: [
                              BoxShadow(
                                  offset: Offset(5, 5),
                                  blurRadius: 5,
                                  color: Color.fromRGBO(0, 0, 0, 0.5)),
                            ]),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 150.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: RawMaterialButton(
                                padding:
                                    EdgeInsets.only(left: 8.0, right: 12.0),
                                animationDuration: Duration(seconds: 3),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50)),
                                onPressed: () => {},
                                child: Row(
                                  children: <Widget>[
                                    Image.asset("asset/icons/icon-facebook.png",
                                        height: 25, fit: BoxFit.contain),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: Text(
                                        "Facebook",
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(255, 255, 255, 1),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                fillColor: Color.fromRGBO(87, 137, 237, 1),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: RawMaterialButton(
                                padding:
                                    EdgeInsets.only(left: 8.0, right: 12.0),
                                animationDuration: Duration(seconds: 3),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50)),
                                onPressed: () => {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LandingPage()))
                                    },
                                child: Row(
                                  children: <Widget>[
                                    Image.asset("asset/icons/icon-google.png",
                                        height: 25, fit: BoxFit.contain),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 13, right: 13),
                                      child: Text(
                                        "Google",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                255, 255, 255, 1)),
                                      ),
                                    ),
                                  ],
                                ),
                                fillColor: Color.fromRGBO(252, 78, 78, 1),
                              ),
                            ),
                          ],
                        ),
                        Center(
                          child: RawMaterialButton(
                            animationDuration: Duration(seconds: 3),
                            onPressed: () => {},
                            padding: EdgeInsets.only(left: 8.0, right: 12.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image.asset("asset/icons/icon-smartphone.png",
                                    height: 25, fit: BoxFit.contain),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Text(
                                    "Số điện thoại",
                                    style: TextStyle(
                                        color:
                                            Color.fromRGBO(255, 255, 255, 1)),
                                  ),
                                ),
                              ],
                            ),
                            fillColor: Color.fromRGBO(150, 150, 150, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            "VNPT@2019",
                            style: TextStyle(shadows: [
                              Shadow(
                                  blurRadius: 5,
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  offset: Offset(1, 1))
                            ], color: Color.fromRGBO(255, 255, 255, 1)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
