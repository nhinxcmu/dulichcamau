import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DoiTuongService {
  Stream<QuerySnapshot> layDs() {
    return Firestore.instance
        .collection("doiTuongs")
        .snapshots();
  }

  void themDiaDanh(DoiTuong doiTuong, Function(DocumentReference) callBack) {
    Firestore.instance.collection("doiTuongs").add({
      "maDoiTuong": doiTuong.maDoiTuong,
      "tenDoiTuong": doiTuong.tenDoiTuong,
      "diaChi": doiTuong.diaChi,
      "moTa": doiTuong.moTa,
      "hinhAnh": doiTuong.hinhAnh,
      "commentCount": doiTuong.commentCount,
    }).then((documentReference) {
      callBack(documentReference);
    });
  }

  void suaDiaDanh(DoiTuong doiTuong) {}

  int xoaDiaDanh(DoiTuong doiTuong) {
    return 0;
  }
}
