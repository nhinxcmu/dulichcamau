import 'package:flutter/material.dart';

class CustomAppBar extends StatefulWidget {
  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          IconButton(
            padding:  const EdgeInsets.all(4.0),
            highlightColor: Color.fromRGBO(255, 255, 255, 1),
            splashColor:  Color.fromRGBO(255, 255, 255, 1),
            icon: Icon(
              Icons.menu,
              color: Color.fromRGBO(3, 150, 74, 1),
            ),
            onPressed: () => {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Show menu"),
                  ))
                },
          )
        ],
      ),
    );
  }
}
