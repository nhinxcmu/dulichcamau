

import 'package:dulichcamau/components/Item.dart';
import 'package:dulichcamau/models/Category.dart';
import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:dulichcamau/pages/diaDanhPage.dart';
import 'package:flutter/material.dart';

class DanhSachDiaDanhPage extends StatefulWidget {
  Category category;
  AnimationController animationController;
  DanhSachDiaDanhPage({this.animationController, this.category});
  @override
  _DanhSachDiaDanhPageState createState() => _DanhSachDiaDanhPageState();
}

class _DanhSachDiaDanhPageState extends State<DanhSachDiaDanhPage> {
  DoiTuong diaDanh;
  double diaDanhContentHeight = 0;
  PageController pageController = new PageController(
    initialPage: 0,
    keepPage: true,
    viewportFraction: 0.8,
  );
  int currentIndex = 0;
  @override
  void initState() {
    super.initState();
    diaDanh = DoiTuong(
      commentCount: 0,
      diaChi: "",
      hinhAnh: "",
      isFavourite: false,
      maDoiTuong: -1,
      moTa: "",
      tenDoiTuong: "",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    gradient: widget.category.gradient,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 10, color: Color.fromRGBO(0, 0, 0, 0.7))
                    ]),
                height: MediaQuery.of(context).size.height * (2 / 3) + 20,
                child: PageView(
                  onPageChanged: (index) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                  controller: pageController,
                  scrollDirection: Axis.horizontal,
                  children: widget.category.diaDanhs.map((_diaDanh) {
                    return Item(
                        onClickCallBack: () {
                          setState(() {
                            diaDanh = _diaDanh;
                          });
                        },
                        doiTuong: _diaDanh,
                        isActive: currentIndex ==
                            widget.category.diaDanhs.indexOf(_diaDanh));
                  }).toList(),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: Hero(
              tag: "diaDanhContent",
              child: DiaDanhContent(
                diaDanh: this.diaDanh,
                headerHeight: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AnimatedBackground extends StatefulWidget {
  AnimationController animationController;
  AnimatedBackground(this.animationController);
  @override
  _AnimatedBackgroundState createState() => _AnimatedBackgroundState();
}

class _AnimatedBackgroundState extends State<AnimatedBackground> {
  Animation animation;

  @override
  void initState() {
    super.initState();

    animation = CurvedAnimation(
        parent: widget.animationController, curve: Curves.easeInOut);
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedParticle(animation: animation);
  }
}

class AnimatedParticle extends AnimatedWidget {
  const AnimatedParticle({
    Key key,
    @required this.animation,
  }) : super(key: key, listenable: animation);

  final Animation animation;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: -100,
      top: 100 +
          animation.value * (MediaQuery.of(context).size.height / 3 - 100),
      child: Container(
        width: 200,
        height: 200,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromRGBO(0, 128, 0, 1),
        ),
      ),
    );
  }
}
