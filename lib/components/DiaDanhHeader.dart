import 'dart:ui';

import 'package:dulichcamau/models/Category.dart';
import 'package:flutter/material.dart';


class DiaDanhHeader extends StatefulWidget {
  final Category category;
  final AnimationController animationController;
  const DiaDanhHeader({Key key, this.category, this.animationController})
      : super(key: key);

  @override
  _DiaDanhHeaderState createState() => _DiaDanhHeaderState();
}

class _DiaDanhHeaderState extends State<DiaDanhHeader> {
  Animation animation;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animation = CurvedAnimation(
        parent: widget.animationController, curve: Curves.ease,reverseCurve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBackdropFilter(
      animation: animation,
      category: widget.category,
    );
  }
}

class AnimatedBackdropFilter extends AnimatedWidget {
  final Category category;
  final Animation animation;
  const AnimatedBackdropFilter({Key key, this.animation, this.category})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: -(animation.value)*(MediaQuery.of(context).size.height/20),
      left: 0,
      child: Opacity(
        opacity: 1 - animation.value*0.8,
        child: Container(
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Image.asset(
                category.image,
                height: MediaQuery.of(context).size.height / 3 + 70,
                fit: BoxFit.cover,
                alignment: Alignment.bottomCenter,
              ),
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Text(
                        category.name,
                        style: TextStyle(
                            letterSpacing: 2,
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            shadows: [
                              Shadow(
                                blurRadius: 10,
                                color: Color.fromRGBO(0, 0, 0, 1),
                                offset: Offset(0, 0),
                              ),
                              Shadow(
                                blurRadius: 10,
                                color: Color.fromRGBO(0, 0, 0, 1),
                                offset: Offset(0, 0),
                              ),
                            ]),
                      ),
                    ),
                  ],
                ),
              ),
              // new AnimatedBackdropFilter(
              //   animation: animation,
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
