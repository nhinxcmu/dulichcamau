import 'package:dulichcamau/thongSo.dart';
import 'package:flutter/material.dart';

class SidePageBar extends StatefulWidget {
  final VoidCallback changePageCallBack;
  final PageController controller;
  final int currentIndex;

  const SidePageBar(
      {Key key, this.controller, this.currentIndex, this.changePageCallBack})
      : super(key: key);
  @override
  _SidePageBarState createState() => _SidePageBarState();
}

class _SidePageBarState extends State<SidePageBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          PageBarItem(
            color:  Color.fromRGBO(3, 150, 74, 1),
            icon: Icons.place,
            pageName: "Địa danh",
            isActive: widget.currentIndex == 0,
            onTap: () {
              widget.controller
                  .animateToPage(0, curve: Curves.ease, duration: duration);
            },
          ),
          PageBarItem(
            color:  Color.fromRGBO(251, 153, 2, 1),
            icon: Icons.restaurant_menu,
            pageName: "Ẩm thực",
            isActive: widget.currentIndex == 1,
            onTap: () {
              widget.controller
                  .animateToPage(1, curve: Curves.ease, duration: duration);
            },
          ),
          PageBarItem(
            color:  Color.fromRGBO(134, 1, 175, 1),
            icon: Icons.local_florist,
            pageName: "Sự kiện",
            isActive: widget.currentIndex == 2,
            onTap: () {
              widget.controller
                  .animateToPage(2, curve: Curves.ease, duration: duration);
            },
          ),
        ],
      ),
    );
  }
}

class PageBarItem extends StatefulWidget {
  final Color color;
  final VoidCallback onTap;
  final bool isActive;
  final String pageName;
  final IconData icon;
  const PageBarItem(
      {Key key, this.onTap, this.isActive, this.icon, this.pageName,this.color})
      : super(key: key);

  @override
  _PageBarItemState createState() => _PageBarItemState();
}

class _PageBarItemState extends State<PageBarItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 5.0,
        right: 5.0,
      ),
      child: InkWell(
        onTap: () {
          widget.onTap();
        },
        child: AnimatedContainer(
          duration: duration,
          curve: Curves.ease,
          width: widget.isActive ? 100 : 31,
          height: 30,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Color.fromRGBO(255, 255, 255, 0.8),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: ClipRect(
              child: OverflowBox(
                alignment: Alignment.centerLeft,
                minHeight: 0,
                minWidth: 0,
                maxWidth: double.infinity,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:widget.color,// Color.fromRGBO(3, 150, 74, 1),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(4),
                        child: Icon(
                          widget.icon,
                          color: Colors.white,
                          size: 15,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 4),
                      child: AnimatedOpacity(
                        opacity: widget.isActive ? 1 : 0,
                        duration: duration,
                        child: Text(
                          widget.pageName,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
