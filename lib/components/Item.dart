
import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:dulichcamau/pages/diaDanhPage.dart';
import 'package:dulichcamau/thongSo.dart';
import 'package:flutter/material.dart';

class Item extends StatefulWidget {
  final VoidCallback onClickCallBack;
  final bool isActive;
  final DoiTuong doiTuong;
  const Item({Key key, this.doiTuong, this.isActive, this.onClickCallBack})
      : super(key: key);

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item> {
  bool isLiked = false;

  double diaDanhContentHeight = 0.0;

  @override
  Widget build(BuildContext context) {
    sangTrangDiaDanh() {
      Navigator.push(
        context,
        PageRouteBuilder(
          transitionsBuilder: (context, animation1, animation2, child) {
            return FadeTransition(opacity: animation1, child: child);
          },
          transitionDuration: duration,
          pageBuilder: (context, animation1, animation2) {
            return DiaDanhPage(
              diaDanh: widget.doiTuong,
            );
          },
        ),
      );
    }

    var ratio = widget.isActive ? 1.0 : 0.8;
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Center(
        child: AnimatedContainer(
          curve: Curves.linear,
          duration: Duration(milliseconds: 270),
          height: MediaQuery.of(context).size.height * 3 / 5 * ratio,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                blurRadius: 5,
                color: Color.fromRGBO(0, 0, 0, 0.3),
                offset: Offset(2, 4))
          ]),
          child: Stack(
            children: [
              InkWell(
                onTap: () {
                  widget.onClickCallBack();
                  sangTrangDiaDanh();
                },
                child: Hero(
                  tag: "dd" + widget.doiTuong.maDoiTuong.toString(),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        image: DecorationImage(
                            image: AssetImage(this.widget.doiTuong.hinhAnh),
                            fit: BoxFit.cover,
                            repeat: ImageRepeat.noRepeat)),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(11),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.black45, Colors.transparent])),
                height: 75,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      left: 0,
                      top: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(11.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.doiTuong.tenDoiTuong,
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                  fontSize: 19,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              widget.doiTuong.diaChi,
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                  fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                width: (MediaQuery.of(context).size.width * (0.8) - 30) * ratio,
                bottom: 0,
                child: Container(
                  height: 75,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      gradient: LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: [Colors.black45, Colors.transparent])),
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        child: IconButton(
                          onPressed: () => {
                                this.setState(() {
                                  isLiked = !isLiked;
                                })
                              },
                          icon: Icon(
                            isLiked ? Icons.favorite : Icons.favorite_border,
                            color: Color.fromRGBO(255, 255, 255, 0.8),
                          ),
                        ),
                        left: 0,
                        bottom: 0,
                      ),
                      Positioned(
                        right: 11,
                        bottom: 0,
                        child: Container(
                            child: Row(
                          children: <Widget>[
                            IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(
                                Icons.message,
                                color: Color.fromRGBO(255, 255, 255, 0.8),
                              ),
                              onPressed: () {
                                sangTrangDiaDanh();
                              },
                            ),
                            Text(
                              widget.doiTuong.commentCount.toString(),
                              style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 0.8),
                                fontSize: 15,
                              ),
                            )
                          ],
                        )),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
