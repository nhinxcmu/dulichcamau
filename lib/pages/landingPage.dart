
import 'package:dulichcamau/components/CustomAppBar.dart';
import 'package:dulichcamau/components/DanhSachDiaDanh.dart';
import 'package:dulichcamau/components/DiaDanhHeader.dart';
import 'package:dulichcamau/components/SidePageBar.dart';
import 'package:dulichcamau/models/Category.dart';
import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:dulichcamau/thongSo.dart';
import 'package:flutter/material.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  Category currentCategory;
  List<Category> categories = [
    Category(
      gradient: LinearGradient(
        colors: [
          Color.fromRGBO(0, 204, 102, 1),
          Color.fromRGBO(51, 204, 204, 1),
          Color.fromRGBO(51, 153, 255, 1),
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
      image: "asset/images/background-mainpage.jpg",
      diaDanhs: [
        DoiTuong(
            commentCount: 33,
            diaChi: "Thành phố Cà Mau",
            hinhAnh: "asset/images/diadiem1.jpg",
            isFavourite: false,
            maDoiTuong: 1,
            moTa:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
            tenDoiTuong: "Tượng đài Cà Mau"),
        DoiTuong(
            commentCount: 33,
            diaChi: "Thành phố Cà Mau",
            hinhAnh: "asset/images/diadiem1.jpg",
            isFavourite: false,
            maDoiTuong: 2,
            moTa:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
            tenDoiTuong: "Tượng đài Cà Mau"),
        DoiTuong(
            commentCount: 33,
            diaChi: "Thành phố Cà Mau",
            hinhAnh: "asset/images/diadiem1.jpg",
            isFavourite: false,
            maDoiTuong: 3,
            moTa:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
            tenDoiTuong: "Tượng đài Cà Mau"),
      ],
      name: "Địa danh",
    ),
    Category(
      gradient: LinearGradient(
        colors: [
          Color.fromRGBO(255, 140, 26, 1),
          Color.fromRGBO(255, 204, 102, 1),
          Color.fromRGBO(255, 102, 102, 1),
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
      image: "asset/images/dacsan1.png",
      diaDanhs: [
        DoiTuong(
          tenDoiTuong: "Cá lóc nướng trui",
          commentCount: 33,
          diaChi: "Mũi Cà Mau",
          hinhAnh: "asset/images/dacsan1.jpg",
          isFavourite: false,
          maDoiTuong: 4,
          moTa:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
        ),
        DoiTuong(
            commentCount: 33,
            diaChi: "Thành phố Cà Mau",
            hinhAnh: "asset/images/dacsan1.jpg",
            isFavourite: false,
            maDoiTuong: 5,
            moTa:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
            tenDoiTuong: "Bánh tầm cay"),
        DoiTuong(
            commentCount: 33,
            diaChi: "Thành phố Cà Mau",
            hinhAnh: "asset/images/dacsan1.jpg",
            isFavourite: false,
            maDoiTuong: 6,
            moTa:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere erat sit amet tortor facilisis, et dapibus tortor luctus. Donec sit amet ipsum vitae leo lobortis laoreet. Suspendisse placerat elit a porttitor ultricies. Suspendisse nec vestibulum dui. Etiam maximus pretium porta. In dui nisl, maximus sodales egestas vel, facilisis a metus. Suspendisse vitae mauris ac lacus hendrerit ultrices. Nullam nulla lorem, sollicitudin in lacinia vitae, congue in magna. Maecenas eu nisl ultrices, mollis ante ut, tempus sapien. Proin eu lorem risus. Cras luctus tortor libero, at tempus urna pellentesque ut. Duis eu semper orci. Praesent vel justo sed felis aliquet vehicula ac ut tortor. Curabitur egestas massa nec velit porta tempor quis id sapien.",
            tenDoiTuong: "Bánh tầm cay"),
      ],
      name: "Ẩm thực",
      color: Color.fromRGBO(255, 153, 0, 1),
    )
  ];
  double diaDanhContentHeight = 0;
  int currentIndex = 0;
  double opacity = 1;
  PageController pageController = new PageController(
    initialPage: 0,
    keepPage: false,
  );
  AnimationController animationController;
  @override
  void initState() {
    super.initState();
    currentCategory = categories[0];
    animationController = AnimationController(
      vsync: this,
      duration: duration * (2/3),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              Color.fromRGBO(242, 242, 242, 1),
              Color.fromRGBO(230, 230, 230, 1),
              Color.fromRGBO(200, 200, 200, 1),
              Color.fromRGBO(180, 180, 180, 1),
              //Color.fromRGBO(0, 255, 0, 1),
              //Color.fromRGBO(0, 204, 255, 1),
              //Color.fromRGBO(3, 150, 74, 1),
              //Color.fromRGBO(3, 150, 74, 1),
              // Colors.white,
              // Colors.white
            ])),
        child: Stack(
          children: [
            DiaDanhHeader(
              category: currentCategory,
              animationController: animationController,
            ),
            Positioned(
              bottom: -20,
              left: 0,
              child: GestureDetector(
                onVerticalDragUpdate: (DragUpdateDetails details) {},
                onVerticalDragEnd: (DragEndDetails details) {
                  animationController.forward().then((f) {
                    animationController.reverse(from: 2/3);
                  });
                  pageController
                      .animateToPage(
                          currentIndex +
                              (details.velocity.pixelsPerSecond.dy > 0
                                  ? -1
                                  : 1),
                          duration: duration,
                          curve: Curves.ease)
                      .then((onValue) {});
                },
                child: Container(
                  height: MediaQuery.of(context).size.height + 20,
                  width: MediaQuery.of(context).size.width,
                  child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    onPageChanged: (index) {
                      this.setState(() {
                        currentIndex = index;
                        setState(() {
                          currentCategory = categories[index];
                        });
                      });
                    },
                    scrollDirection: Axis.vertical,
                    controller: pageController,
                    children: <Widget>[
                      DanhSachDiaDanhPage(
                        category: categories[0],
                        animationController: animationController,
                      ),
                      DanhSachDiaDanhPage(
                        category: categories[1],
                        animationController: animationController,
                      ),
                      Container(
                        color: Colors.transparent,
                        child: Text("page 3"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).size.height / 2 -
                  45,
              right: 0,
              child: SidePageBar(
                currentIndex: this.currentIndex,
                controller: pageController,
              ),
            ),
            CustomAppBar(),
          ],
        ),
      ),
    );
  }
}
