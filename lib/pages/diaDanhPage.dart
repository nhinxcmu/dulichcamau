
import 'package:dulichcamau/components/CustomAppBar.dart';
import 'package:dulichcamau/models/DiaDanh.dart';
import 'package:dulichcamau/thongSo.dart';
import 'package:flutter/material.dart';

class DiaDanhPage extends StatefulWidget {
  final DoiTuong diaDanh;
  const DiaDanhPage({Key key, this.diaDanh}) : super(key: key);
  @override
  _DiaDanhPageState createState() => _DiaDanhPageState();
}

class _DiaDanhPageState extends State<DiaDanhPage> {
  double headerHeight = 1.5;
  double alignmentX =0;
  @override
  void initState() {
    super.initState();
    // accelerometerEvents.listen((AccelerometerEvent event) {
    //   if(alignmentX-event.x>-2||alignmentX-event.x>2 )
    //   setState(() {
    //    alignmentX=event.x; 
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              child: Hero(
                tag: "dd" + widget.diaDanh.maDoiTuong.toString(),
                child: AnimatedContainer(
                  duration: duration,
                  curve: Curves.ease,
                  width: MediaQuery.of(context).size.width,
                  height:
                      MediaQuery.of(context).size.height / this.headerHeight +
                          50,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage(widget.diaDanh.hinhAnh),
                    alignment: Alignment(alignmentX, 0),
                    fit: BoxFit.cover,
                  )),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Hero(
                tag: "diaDanhContent",
                child: GestureDetector(
                  onVerticalDragUpdate: (detail) {
                    if (detail.primaryDelta < 0) {
                      setState(() {
                        headerHeight = 3;
                      });
                    }
                    if (detail.primaryDelta > 0) {
                      setState(() {
                        headerHeight = 1.5;
                      });
                    }
                  },
                  child: new DiaDanhContent(
                      headerHeight: headerHeight, diaDanh: widget.diaDanh),
                ),
              ),
            ),
            Positioned(
              top: 0,
              child: CustomAppBar(),
            )
          ],
        ),
      ),
    );
  }
}

class DiaDanhContent extends StatelessWidget {
  final DoiTuong diaDanh;
  const DiaDanhContent({
    Key key,
    @required this.diaDanh,
    @required this.headerHeight,
  }) : super(key: key);

  final double headerHeight;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: duration,
      curve: Curves.ease,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height -
          MediaQuery.of(context).size.height / this.headerHeight,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              blurRadius: 5,
              color: Color.fromRGBO(0, 0, 0, 0.5),
              offset: Offset(0, -2))
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        color: Color.fromRGBO(240, 240, 240, 1),
      ),
      child: ClipRect(
        child: OverflowBox(
          alignment: FractionalOffset.topCenter,
          minHeight: 0,
          minWidth: 0,
          maxHeight: double.infinity,
          child: Padding(
            padding: const EdgeInsets.only(top: 13, left: 11),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Center(
                    child: Container(
                      width: 75,
                      height: 5,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  child: Text(
                    diaDanh.tenDoiTuong,
                    style: TextStyle(
                      fontSize: 30,
                      letterSpacing: 1.2,
                      color: Color.fromRGBO(0, 0, 0, 0.7),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 4, top: 3),
                  child: Material(
                    color: Colors.transparent,
                    child: Text(
                      diaDanh.diaChi,
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 4, top: 5, right: 16),
                  child: Material(
                    color: Colors.transparent,
                    child: Text(
                      diaDanh.moTa,
                      style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.7),
                      ),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
